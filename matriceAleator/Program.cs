﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matriceAleator
{
    class Program
    {
        static void Main(string[] args)
        {
            int m, n;
            Console.Write("Introduceti m: ");
            m = int.Parse(Console.ReadLine());
            Console.Write("Introduceti n: ");
            n = int.Parse(Console.ReadLine());
            int[,] matrice = new int[m, n];
            Random rand = new Random();
            int nr;
            int sumaNormala = 0;
            int sumaInversa = 0;
            int[] frecventa = new int[10];
            for (int i = 0; i < matrice.GetLength(0); i++)
            {
                for (int j = 0; j < matrice.GetLength(1); j++)
                {
                    nr = rand.Next(1, 10);
                    matrice[i, j] = nr;
                    frecventa[matrice[i, j] - 1]++;
                    if (i == j)
                        sumaNormala += matrice[i, j];
                    else
                        if (i + j == matrice.GetLength(0))
                        sumaInversa += matrice[i, j];
                }
            }
            Console.WriteLine("Matricea copulata:");
            for (int i = 0; i < matrice.GetLength(0); i++)
            {
                for (int j = 0; j < matrice.GetLength(1); j++)
                    Console.Write(matrice[i, j] + " ");
                Console.WriteLine();
            }
            Console.WriteLine();
            if (m == n)
            {
                Console.Write("Suma pe diagonala normala este: " + sumaNormala);
                Console.WriteLine();
                Console.Write("Suma pe diagonala inversa este: " + sumaInversa);
                Console.WriteLine("\n");
            }
            else
            {
                Console.WriteLine("Matricea nu este patratica, deci nu se pot calcula sumele pe diagonale.");
                Console.WriteLine();
            }
            for(int i = 0; i < 10; i++)
                if(frecventa[i] > 0)
                    Console.WriteLine("Numarul " + (i+1) + " apare in matrice de " + frecventa[i] + " ori.");
            Console.ReadKey();
        }
    }
}
